# SOA labs: 2. Introduce Eureka
## Use Eureka as service registry and client-side service discovery

### Reproduce steps

1. Clone the project
   * `git clone git@gitlab.com:soa-labs/2018-2.git`

2. Build all docker images:
   * `./build-images.sh`

3. Start mysql (and wait for it to start):
   * `docker-compose up -d mysql`

4. Ensure mysql container is up and running:
   * docker ps
   * docker logs <container-id> 

5. Start eureka (and wait for it to start):
   * `docker-compose up -d eureka`
   * docker logs <container-id> 

6. See eureka in web browser, there should be no instances:
  * `http://localhost/eureka`

7. Start other containers defined in docker-compose:
   * `docker-compose up -d`

8. See instances in eureka, there should be 1 instance of app1 and app2:
   * `http://localhost/eureka`

9. Scale app1 with 3 instances:
   * `docker-compose scale app1=3`

10. See instances in eureka, there should be 3 instances of app1:
   * `http://localhost/eureka`

11. Downscale app1 with 1 instance:
   * `docker-compose scale app1=1`

12. See instances in eureka, there should be 1 instance of app1:
   * `http://localhost/eureka`

13. Shutdown containers:
   * `docker-compose down`
